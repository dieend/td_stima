'''
Created on Sep 21, 2012

@author: dieend
'''
from pygame.sprite import Sprite
import math
from Controller.ModelController import modelController  
import pygame
from Other import Const
from Controller.MasterController import MasterController

CREATING = 1
STANDBY = 2
SHOOTING = 3


class GameObject(Sprite):
    def __init__(self, dictionary):
        super(GameObject, self).__init__()

        self._x = dictionary.get('x',0)
        self._y = dictionary.get('y',0)
        objClass = dictionary['objectClass']
        objType = dictionary['objectType']
        self._type = modelController.objectType[objClass][objType]

class BulletObject(GameObject):
    def __init__(self,dictionary):
        super(BulletObject, self).__init__(dictionary)
        self._target = dictionary['target']
        self._damage = self._type._damage
        self._speed = self._type._speed
        self._element = self._type._element
        
    def explode(self):
        if self._target._element.stronger(self._element):
            self._target._health -= self._damage * 0.5
        elif self._target._element.weaker(self._element):
            self._target._health -= self._damage * 1.5
        else:
            self._target._health -= self._damage 

    def update(self):
        self._updatePosition()
        self.image = self._type._image
        self.rect = self.image.get_rect()
        self.rect.left = self._x
        self.rect.top = self._y

    def _updatePosition(self):
        targetx = self._target.rect.centerx
        targety = self._target.rect.centery
        width = abs(self._x - targetx)
        height = abs(self._y - targety)
        if width < self._speed and height < self._speed:
            self.explode()
            self.kill()
            return
        numerator = 0
        if width > height:
            if self._x < targetx: self._x += self._speed
            else: self._x -= self._speed
            dx = 0;
            if self._y < targety: dy = 1
            else : dy = -1
            denumerator = width
            ratio = height
        else:
            if self._y < targety: self._y += self._type._speed
            else: self._y -= self._speed
            denumerator = height
            ratio = width
            dy = 0;
            if self._x < targetx: dx = 1
            else : dx = -1

        for i in range(self._speed):
            numerator += ratio
            if (numerator > denumerator/2):
                self._x += dx
                self._y += dy
            numerator %= denumerator

class TowerObject(GameObject):
    def __init__(self, dictionary):
        super(TowerObject, self).__init__(dictionary)
        self._level = dictionary['level']
        self._price = self._type._price
        self._state = CREATING
        self._standby_time = pygame.time.get_ticks()
        self.radius = self._type._searchRadius

    def update(self, event):
        self.image = self._type._image 
        self.rect = self._type._image.get_rect()
        self.rect.left = self._x + ((Const.GRID_SIZE-self.rect.width) / 2)
        self.rect.top = self._y + ((Const.GRID_SIZE-self.rect.width) / 2)
        currentTime = pygame.time.get_ticks()
        if self._state == STANDBY:
            target = self._searchArea()
            if target:
                self._state = SHOOTING
                self._shoot(target)
                self._standbyTime = currentTime + self._type._bulletType._delayBetweenShot
        elif self._state == CREATING:
            self._state = STANDBY
            
        elif self._state == SHOOTING:
            if self._standbyTime < currentTime:
                self._state = STANDBY
        else:
            raise Exception, "Invalid state"

    def _searchArea(self):
        inRangeMonsters = pygame.sprite.spritecollide(self, modelController.monsterGroup, False, pygame.sprite.collide_circle)
        return inRangeMonsters

    def _shoot(self,target):
        farthest = -1;
        toShot = None
        for monster in target:
            tmp = math.hypot(monster._x-self._x, monster._y-self._y)
            if farthest==-1:
                farthest = tmp
                toShot = monster
            elif tmp > farthest:
                farthest = tmp
                toShot = monster        
        bullet = BulletObject({'x':self.rect.centerx, 'y':self.rect.centery, 'objectClass':'BulletType', 'objectType':self._type._bulletType._identifier, 'target':toShot})
        modelController.bulletGroup.add(bullet)

class MonsterObject(GameObject):
    def __init__(self, dictionary):
        super(MonsterObject,self).__init__(dictionary)
        self._x = modelController.walkway[0][0]
        self._y = modelController.walkway[0][1]
        self._level = dictionary['level']
        self._moveSpeed = int(max(1,math.log10(self._level)) * self._type._baseSpeed)
        self._targetWalkway = 0
        self._health = self._type._baseHealth + 50*self._level
        self._element = self._type._element
        
    def _arrivedAtTarget(self):
        self.kill()

    def killed(self):
        MasterController._instance.player.money += self._type._prize
        self.kill()
        #TODO(dieend): what happen when killed a monster?
        return
    
    def update(self):
        if self._health <= 0:
            self.killed()
            return            
        self._updatePosition()
        self.image = self._type._image
        self.rect = pygame.Rect(self.image.get_rect())
        self.rect.left = self._x + ((Const.GRID_SIZE-self.rect.width) / 2)
        self.rect.top = self._y + ((Const.GRID_SIZE-self.rect.width) / 2)
        
    def _updatePosition(self):
        if self._targetWalkway >= len(modelController.walkway):
            self._arrivedAtTarget()
            return
        targetx, targety = modelController.walkway[self._targetWalkway]
        width = abs(self._x - targetx)
        height = abs(self._y - targety)
        if width < self._moveSpeed and height < self._moveSpeed:
            self._targetWalkway+=1
            return
        
        numerator = 0
        if width > height:
            if self._x < targetx: self._x += self._moveSpeed
            else: self._x -= self._moveSpeed
            dx = 0;
            if self._y < targety: dy = 1
            else : dy = -1
            denumerator = width
            ratio = height
        else:
            if self._y < targety: self._y += self._moveSpeed
            else: self._y -= self._moveSpeed
            denumerator = height
            ratio = width
            dy = 0;
            if self._x < targetx: dx = 1
            else : dx = -1
        
        for i in range(self._moveSpeed):
            numerator += ratio
            if (numerator > denumerator/2):
                self._x += dx
                self._y += dy
            numerator %= denumerator
