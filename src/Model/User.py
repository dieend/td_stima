'''
Created on Sep 26, 2012

@author: dieend
'''
from Controller.ModelController import modelController
from Other import Const

class Player(object):
    def __init__(self):
        '''
        Constructor
        '''
        super(Player,self).__init__()
        #TODO(dieend): read from csv
        self.money = 0
        self.live = 20

    def tryUpgrade(self):
        self.greedyUpgradeCheaper()

    def cheaper(self,a,b):
        mina = a._type.getCheapestPostRequisite()
        minb = b._type.getCheapestPostRequisite()
        
        if mina and minb:
            return mina._price < minb._price
        elif mina:
            return True
        else:
            return False

    def greedyUpgradeCheaper(self):
        allTower = sorted(modelController.towerGroup.sprites(), cmp=self.cheaper)
        for tower in allTower:
            upg = tower._type.getCheapestPostRequisite()
            if upg :
                if self.money - upg._price > 0:
                    tower._type = upg
                    self.money -= upg._price

class Enemy(object):
    def __init__(self):
        '''
        Constructor
        '''
        super(Enemy,self).__init__()
        #TODO(dieend): read from csv
        self._baseMoney = 1000
        self._wave = 0
        live = 0
    def trySpawn(self):
        self._wave += 1
        return self.enemyGreedyByLowestPrice(self._baseMoney)
    
    def enemyGreedyByLowestPrice(self, money):
        allEnemy = sorted(modelController.objectType['MonsterType'].values(), key=lambda x:x._price)
        print allEnemy
        ret = []
        i = 0
        while money - allEnemy[i]._price > 0 and i<len(allEnemy):
            ret.append(allEnemy[i]._identifier)
            money -= allEnemy[i]._price
            if len(ret) % Const.MAX_SAME_TYPE_IN_A_WAVE == 0:
                i+=1
        return ret