import string
from Other import Utilities, Const
import pygame
from Controller.ModelController import modelController
#from Controller import MasterController


class GameObjectType(object):
    '''
    Base Class that define the Game Object 
    '''
    def __init__(self, dictionary):

        super(GameObjectType, self).__init__()
        self._identifier = dictionary['identifier'];
        self._displayName = dictionary.get('displayName',None)
        self._imageName = dictionary.get('imageName', None);
        self._row = int(dictionary.get('row', 0))
        self._column = int(dictionary.get('column', 0))

        self._animationImageName = dictionary.get('animationImage',"")
        self._animationList = dictionary.get('animationList', "")
        self._animationList = [int(x) for x in string.split(self._animationList, ',')]
        
        self._animationWidth = int(dictionary.get('animationWidth', 0))
        self._animationHeight = int(dictionary.get('animationHeight', 0))
        

    def mapIdentifier(self):
        #set image
        if self._imageName:
            print self._imageName
            if "circle" in self._imageName:
                shape, color = string.split(self._imageName, "-")
                self._image = pygame.Surface((self._animationWidth,self._animationHeight))
                pygame.draw.circle(self._image,pygame.Color(color), (self._animationWidth/2,self._animationHeight/2), int(self._animationWidth/2))
            elif "rectangle" in self._imageName:
                shape, color= string.split(self._imageName, "-")
                self._image = pygame.Surface((self._animationWidth,self._animationHeight))
                pygame.draw.rect(self._image, 
                                 pygame.Color(color),
                                 pygame.Rect(self._animationWidth/10,self._animationHeight/10,
                                             self._animationWidth*4/5,self._animationHeight*4/5))
            else:
                self._image = Utilities.loadImage(self._imageName)

        #set animation image
        if self._animationImageName:
            if "circle" in self._animationImageName:
                shape, color = string.split(self._animationImageName, "-")
                self._animationImage = pygame.Surface(self._animationWidth,self._animationHeight)
                pygame.draw.circle(self._animationImage,pygame.Color(color), (self._animationWidth/2,self._animationHeight/2), int(self._animationWidth/2))
                self._animationImageList = [self._animationImage]
            elif "rectangle" in self._animationImageName:
                shape, color= string.split(self._animationImageName, "-")
                self._animationImage = pygame.Surface(self._animationWidth,self._animationHeight)
                pygame.draw.rect(self._animationImage, pygame.Color(color), pygame.Rect(0,0,self._animationWidth/5*4,self._animationHeight/5*4))
                self._animationImageList = [self._animationImage]
            else:
                self._animationImage = Utilities.loadImage(self._animationImageName)
                masterWidth = self._animationImage.get_width()
                self._animationImageList = [self._animationImage.subsurface(
                                            (i*self._animationWidth, 0,
                                             self._animationWidth, self._animationHeight)) 
                                            for i in range(masterWidth/self._animationWidth)]


class TowerType(GameObjectType):
    '''
    Define TowerType
    '''

    def __init__(self, dictionary):
        '''
        Constructor
        '''
        super(TowerType, self).__init__(dictionary)
        self._price = int(dictionary.get('price', None))
         
        # TODO(dieend): convert to bullet object
        self._bulletTypeId = dictionary.get('bulletType', None)
         
        self._postRequisite = dictionary.get('postRequisite', "")
        self._postRequisite = string.split(self._postRequisite, ',')
        
        self._searchRadius = (int(dictionary.get('searchRadius', 1)) + 1) * Const.GRID_SIZE
        self._creationTime = dictionary.get('creationTime', None)
    def getCheapestPostRequisite(self):
        minb = None
        for upg in self._postRequisite:
            if upg == '': continue
            trial = modelController.objectType['TowerType'][upg]
            if not minb:
                minb = trial
            else: 
                if minb._price > trial._price:
                    minb = modelController.objectType['TowerType'][upg]
        return minb
    def mapIdentifier(self):
        super(TowerType,self).mapIdentifier()
        self._bulletType = modelController.objectType['BulletType'][self._bulletTypeId]

class BulletType(GameObjectType):
    '''
    Define BulletType
    '''
    def __init__(self, dictionary):
        super(BulletType, self).__init__(dictionary)
        self._damage = int(dictionary.get('damage', 0))
        self._speed = int(dictionary.get('speed', 0))
        self._elementId = dictionary.get('element', None) # TODO(dieend): convert to element object
        self._effectRadius = int(dictionary.get('effectRadius', 1))
        self._delayBetweenShot = int(dictionary.get('delayBetweenShot',1))
        
    def mapIdentifier(self):
        super(BulletType, self).mapIdentifier()
        self._element = modelController.objectType['ElementType'][self._elementId]

class MonsterType(GameObjectType):
    def __init__(self, dictionary):
        super(MonsterType,self).__init__(dictionary)
        self._baseHealth = int(dictionary['health'])
        self._price = int(dictionary['price'])
        self._baseSpeed = int(dictionary['speed']) 
        self._elementId = dictionary.get('element')
        self._prize = int(dictionary['prize'])
    def mapIdentifier(self):
        super(MonsterType, self).mapIdentifier()
        self._element = modelController.objectType['ElementType'][self._elementId]

class ElementType(GameObjectType):
    def __init__(self,dictionary):
        super(ElementType, self).__init__(dictionary)
        self._weakness = dictionary.get('weakness','')
        self._weakness = string.split(self._weakness, ',')
        self._strength = dictionary.get('strength','')
        self._strength = string.split(self._strength, ',')

    def stronger(self, element):
        return element._identifier in self._strength

    def weaker(self, element):
        return element._identifier in self._weakness
 
if __name__ == "__main__":
#    x = {"identifier": "a", "animationList" :"1,1"};
#    pprint(x['identifier'])
    x = dict()
    x['identifier'] = 'a'
    x["animationList"] = "1,1"
    for y in x:
        print y
    g = GameObjectType(x)
    print g._animationList