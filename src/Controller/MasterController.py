'''
Created on Sep 21, 2012

@author: dieend
'''
import pygame
from pygame.locals import *
import csv

from ModelController import modelController  
from Other import *
from Model.User import Player, Enemy

EVENT_CREATE_WAVE = USEREVENT + 1
EVENT_SPAWN_MONSTER = USEREVENT + 2
EVENT_UPGRADE_TOWER = USEREVENT + 3
class MasterController(object):
    _instance = None
    def __init__(self):
        super(MasterController,self).__init__()
        pygame.init()

        #load font, prepare values
    #    font = pygame.font.Font(None, 80)
    #    text = 'Start'
    #    
    #    #no AA, no transparancy, normal
    #    renderedFont = font.render(text, 0, fg, bg)
    #    screen.blit(renderedFont, (100, 10))

        from Model.GameObject import TowerObject
        walkColor = 250, 240, 230
        placeColor = 5, 5, 5
        # Map Data
        raw_map = list(csv.reader(open(Const.MAP_NAME)))

        self._infoPoint = len(raw_map)*Const.GRID_SIZE 
        resolution = len(raw_map[0])*Const.GRID_SIZE, len(raw_map)*Const.GRID_SIZE + 100
        self.screen = pygame.display.set_mode(resolution)
        self.background = pygame.Surface(self.screen.get_size())
        pygame.display.set_caption('Tower Defense')
        pygame.mouse.set_visible(1)
        self.background = self.background.convert()
        self.screenWidth = len(raw_map) * Const.GRID_SIZE
        self.screenHeight = len(raw_map[0]) * Const.GRID_SIZE
        for i in range(len(raw_map)):
            for j in range(len(raw_map[i])):
                if raw_map[i][j] == 'O' or raw_map[i][j] == 'S' or raw_map[i][j] == 'F':
                    self.background.fill(walkColor, pygame.Rect(j*Const.GRID_SIZE, i*Const.GRID_SIZE, Const.GRID_SIZE, Const.GRID_SIZE))
                else:
                    self.background.fill(placeColor, pygame.Rect(j*Const.GRID_SIZE, i*Const.GRID_SIZE, Const.GRID_SIZE, Const.GRID_SIZE))
        
        self.screen.blit(self.background, self.screen.get_rect())
        
        #load font, prepare values
        self.font = pygame.font.Font(None, 20)

        text = 'wave: '
        ren = self.font.render(text, 0, pygame.Color("white"), pygame.Color("black"))
        wavex = 10
        wavey = self._infoPoint+5
        wavew,waveh = self.font.size(text) 
        self.screen.blit(ren, (wavex, wavey))
        self._wavePosition = wavew+wavex+2, wavey
        
        text = 'money: '
        ren = self.font.render(text, 0, pygame.Color("white"), pygame.Color("black"))
        monex = 10
        money = wavey+waveh+5
        monew, moneh = self.font.size(text)
        self.screen.blit(ren, (monex, money))
        self._moneyPosition = monex+monew + 2, money 
        
        pygame.display.flip()
        
        modelController.initialize()
        modelController.mapIdentifierToObject()
        
        modelController.walkway = Utilities.loadWalkwayPathFromMap(raw_map, Const.GRID_SIZE)
        
        #Game Object
        raw_object = Utilities.loadTowerLocation(raw_map, Const.GRID_SIZE)

        for obj in raw_object:
            print {'x':obj[1][1],'y':obj[1][0],'objectClass':'TowerType', 'objectType':obj[0], 'level':1}
            TO = TowerObject({'x':obj[1][1],'y':obj[1][0],'objectClass':'TowerType', 'objectType':obj[0], 'level':1})
            modelController.towerGroup.add(TO)

        self.player = Player()
        self.enemy = Enemy()


    def play(self):
        from Model.GameObject import MonsterObject
        running = True
        clock = pygame.time.Clock()
        pygame.time.set_timer(EVENT_CREATE_WAVE, Const.WAVE_DELAY)
        pygame.time.set_timer(EVENT_UPGRADE_TOWER, 10)
        pygame.event.post(pygame.event.Event(EVENT_CREATE_WAVE))
        counter = 0
        white = pygame.Color("white")
        black = pygame.Color("black")
        while running:
            clock.tick(30)
            modelController.monsterGroup.update()
            pygame.display.update(modelController.monsterGroup.draw(self.screen))
            modelController.monsterGroup.clear(self.screen, self.background)

            modelController.towerGroup.update(None)
            pygame.display.update(modelController.towerGroup.draw(self.screen))
            modelController.towerGroup.clear(self.screen, self.background)

            modelController.bulletGroup.update()
            pygame.display.update(modelController.bulletGroup.draw(self.screen))
            modelController.bulletGroup.clear(self.screen, self.background)

            self.screen.blit(self.font.render(str(self.enemy._wave)+"    ", 0, white, black), self._wavePosition)
            self.screen.blit(self.font.render(str(self.player.money)+"    ", 0, white, black), self._moneyPosition)
            pygame.display.update(pygame.Rect(0,self._infoPoint,self.screenWidth,100))
            
            
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False
                elif event.type == MOUSEBUTTONDOWN:
                    pass
                elif event.type == EVENT_CREATE_WAVE:
                    print "create wave"
                    pygame.time.set_timer(EVENT_SPAWN_MONSTER, 1000)
                    pygame.event.post(pygame.event.Event(EVENT_SPAWN_MONSTER))
                    self.monsterinWave = self.enemy.trySpawn()
                elif event.type == EVENT_SPAWN_MONSTER:
                    if counter>=len(self.monsterinWave):
                        counter = 0
                        pygame.time.set_timer(EVENT_SPAWN_MONSTER,0)
                        continue
                    mo = MonsterObject({'objectClass':'MonsterType', 'objectType':self.monsterinWave[counter], 'level':1})
                    modelController.monsterGroup.add(mo)
                    counter = counter + 1
                    print "spawn monster"
                elif event.type == EVENT_UPGRADE_TOWER:
                    self.player.tryUpgrade()
                else:
                    pass

if __name__ == "__main__":
    pass