'''
Created on Sep 22, 2012

@author: dieend
'''

from Other import Utilities
import pygame

class ModelController(object):
    _instance = None
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(ModelController, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance
    def __init__(self):
        print "initiate Model"
        super(ModelController,self).__init__()
        self.objectType = {}
        self.monsterGroup = pygame.sprite.RenderUpdates()
        self.towerGroup = pygame.sprite.RenderUpdates()
        self.bulletGroup = pygame.sprite.RenderUpdates()
        self.walkway = []

    def initialize(self):
        from Model.GameObject import TowerObject
        #load file data
        csvs = Utilities.loadCSVasDict("file_def.csv")
        # GameObjectType
        for line in csvs:
            #load tower data
            _objectData = Utilities.loadCSVasDict(line['filename'])
            for _datum in _objectData:
                _objType = _datum['classType']
                _objClass = Utilities.getClassFromString(_objType)
                _objId = _datum['identifier']
                if not _objType in self.objectType:
                    self.objectType[_objType] = {}
                self.objectType[_objType][_objId] = _objClass(_datum)

    def mapIdentifierToObject(self):
        for objClasses in self.objectType.itervalues():
            for objType in objClasses.itervalues():
                objType.mapIdentifier()

if not ModelController._instance: 
    modelController = ModelController()