'''
Created on Sep 19, 2012

@author: dieend
'''

IMAGE_FOLDER = "Other/resource/image"
SOUND_FOLDER = "Other/resource/sound"
DEFINITION_FOLDER = "Other/resource/object_definition"
GAME_OBJECT_MODULE = "Model.GameObjectType."
MAP_NAME = "Other/resource/object_definition/map.csv"
GRID_SIZE = 40
MAX_SAME_TYPE_IN_A_WAVE = 5
WAVE_DELAY = 30000 #in milisecond