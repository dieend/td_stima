'''
Created on Sep 19, 2012

@author: dieend
'''

"""
A singleton that handles everything about utilities.
"""
    
import Const
import os, pygame

def loadWalkwayPathFromMap(rawmap, multiplier):
    r = len(rawmap)
    c = len(rawmap[0])
    print rawmap
    for i in range(r):
        for j in range(c):
            if (rawmap[i][j] == 'S'): s = (i,j)
    q = [(s,None)]
    done = {}
    done[s] = True
    dx = [1,0,-1,0]
    dy = [0,1,0,-1]
    qc = 0
    found = False
    while qc < len(q) and not found:
        (nowx, nowy), prev = q[qc]
        for i in range(4):
            nx = nowx+dx[i]
            ny = nowy+dy[i]
            if 0<=nx and nx < r and 0<=ny and ny<c and not done.get((nx,ny),None):
                if rawmap[nx][ny]=="O":
                    done[(nx,ny)] = True
                    q.append(((nx,ny), qc))
                elif rawmap[nx][ny] == "F":
                    done[(nx,ny)] = True
                    q.append(((nx,ny), qc))
                    found = True;
                    break
        qc+=1
    m, prev = q[-1]
    ret = []
    while prev != None:
        ret.append((m[1]*multiplier, m[0]*multiplier))
        m,prev = q[prev]
    ret.append((m[1]*multiplier, m[0]*multiplier))
    ret.reverse()
    return ret

def loadTowerLocation(rawmap, multiplier):
    r = len(rawmap)
    c = len(rawmap[0])
    ret = []
    for i in range(r):
        for j in range(c):
            if rawmap[i][j] != 'O' and rawmap[i][j] != 'S' and rawmap[i][j] != 'F' and rawmap[i][j] != 'X':
                ret.append((rawmap[i][j],(i*multiplier,j*multiplier)))
    return ret
    
def loadImage(name, colorkey=None):
    """
    return pygame.image object. The sound file is in folder IMAGE_FOLDER
    """
    fullname = os.path.join(Const.IMAGE_FOLDER, name)

    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print 'Cannot load image: %s at %s' % (name, os.path.realpath(fullname))
        raise SystemExit, message
    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey, pygame.locals.RLEACCEL)
    return image

def loadSound(name):
    """
    return pygame.mixer.Sound object. The sound file is in folder SOUND_FOLDER
    """
    class NoneSound:
        def play(self): pass
    if not pygame.mixer:
        return NoneSound()
    fullname = os.path.join(Const.SOUND_FOLDER, name)
    try:
        sound = pygame.mixer.Sound(fullname)
    except pygame.error, message:
        print 'Cannot load sound: %s at %s' % (name, os.path.realpath(fullname))
        raise SystemExit, message
    return sound

def getIfExistFromDict(name, dictionary, required = False):
    """
    Add an attribute to var object.
    """ 
    
    exist = name in dictionary
    if required and not exist:
        print "attribute %s not found." % name 
        raise SystemExit,"Atribute %s is required " % name
    else:
        if (exist):
            return dictionary[name]

def loadCSVasDict(name):
    """
    Load object from csv file in DEFINITION_FOLDER
    """
    import csv
    fullname = os.path.join(Const.DEFINITION_FOLDER, name)
    o = csv.DictReader(open(fullname))
    return list(o)

def getClassFromString( kls ):
    kls = Const.GAME_OBJECT_MODULE + kls
    parts = kls.split('.')
    module = ".".join(parts[:-1])
    m = __import__( module )
    for comp in parts[1:]:
        m = getattr(m, comp)            
    return m

if __name__ == "__main__":
    #pygame.init()
    #screen = pygame.display.set_mode((0, 0))
    #loadImage("test.png")
    #loadSound("punch.wav")
    
    #pprint(loadCSVasDict("tower_def.csv"))
    #print getClassFromString("TowerType")
    a = [['S', 'X', 'F', 'X', 'X'], 
         ['O', 'X', 'O', 'O', 'X'], 
         ['O', 'X', 'X', 'O', 'O'], 
         ['O', 'O', 'O', 'X', 'O'], 
         ['X', 'X', 'O', 'X', 'O'], 
         ['O', 'O', 'O', 'X', 'O'], 
         ['O', 'X', 'X', 'X', 'O'], 
         ['O', 'O', 'O', 'O', 'O']]

    print loadWalkwayPathFromMap(a)